package cn.keepbx.coneroller;

import cn.keepbx.TestAnnotationController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jiangzeyin
 * @date 2019/5/9
 */
@TestAnnotationController
public class TestController {

    @RequestMapping(value = "/test")
    public String test() {
        return "test";
    }
}
