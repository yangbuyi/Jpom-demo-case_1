package cn.keepbx;

import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.*;

/**
 * 测试注解
 *
 * @author jiangzeyin
 * @date 2019/5/9
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RestController
public @interface TestAnnotationController {
}
