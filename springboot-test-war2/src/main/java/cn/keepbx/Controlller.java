package cn.keepbx;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jiangzeyin
 * @date 2019/07/21
 */
@RestController
public class Controlller {


    @RequestMapping(value = "/")
    public String test() {
        return "spring-bootwar2";
    }

    @RequestMapping(value = "index")
    public String index() {
        return "test_index";
    }
}
