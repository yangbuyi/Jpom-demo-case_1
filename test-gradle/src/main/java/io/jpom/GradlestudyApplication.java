package io.jpom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradlestudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GradlestudyApplication.class, args);
    }

}
