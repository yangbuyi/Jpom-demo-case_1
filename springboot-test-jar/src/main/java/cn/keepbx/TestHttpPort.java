//package cn.keepbx;
//
//import cn.hutool.http.HttpUtil;
//import cn.hutool.http.server.HttpServerRequest;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @author bwcx_jzy
// * @since 2022/12/4
// */
//@Configuration
//public class TestHttpPort implements InitializingBean {
//    @Override
//    public void afterPropertiesSet() throws Exception {
//
//        HttpUtil.createServer(8888)
//                .addAction("/", (req, res) -> {
//                    res.write("Hello Hutool Server");
//                })
//                .start();
//    }
//}
